function showPassword() {
    const eye = document.getElementById('eye');
    const eyeSlash = document.getElementById('eyeSlash');
    const fieldPassword = document.getElementById('fieldPassword');

    if(eye.display === 'none') {
        eye.style.display ='block';
        eyeSlash.style.display ='none';
        fieldPassword.style.display ='text';
    }else{
        eye.style.display ='none';
        eyeSlash.style.display ='block';
        fieldPassword.style.display ='password';
    }
}

document.getElementById('btn-login').addEventListener('click', function(e){
    e.preventDefault();
    alert('Logado!');
}